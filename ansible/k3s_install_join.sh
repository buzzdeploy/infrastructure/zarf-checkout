#!/bin/bash
# Install k3s and join the nodes on the newly provisioned nodes. 

ansible-playbook k3s_install_join.yml --skip-tags "disjoin,uninstall"
 
# Get the unique number
uniqueid="s"$(grep "unique_number" ../main.tf |awk -F'\"' '{print $2}')
configname="${uniqueid}config"
 
# A grabs the kubeconfig from the controller node and setups up the kubectl locally. 
ansible controllers[0] -m fetch -a "src=/etc/rancher/k3s/k3s.yaml dest=./${configname} flat=yes" --become
CONTROLLER_URL=$(grep controller_url linode.hosts)
ansible localhost -m lineinfile -a "path=./${configname} regexp='    server: https://127.0.0.1:6443' line='    server: {{ controller_url }}' state=present" -e "$CONTROLLER_URL"

# Push to config to an S3 bucket
linode-cli obj put ${configname} k3sfiles --cluster us-east-1
cp linode.hosts ${uniqueid}linode.hosts
linode-cli obj put ${uniqueid}linode.hosts k3sfiles --cluster us-east-1

