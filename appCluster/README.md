
# Generated Module Variables
## Module Data
| Data Name |
| :--- | 
| linode_user |

## Module Resources
| Resource Name |
| :--- | 
| linode_instance |

## Module Variables
| Variable Name | Variable Description | Type | Default |
| :--- | :--- | :---: | ---: |
| linodeapikey | Linode ApiKey | ${string} | None |
| unique_number | Unique number so cluster don't impact eachother | ${string} | 00 |
| archrootpass | Arch Linux Root Password | ${string} | None |
| ssh_key_pub | SSH Public Key | ${string} | None |
| ssh_key | SSH Private Key | ${string} | None |
| linode_username | Username to be used as noted in the Linode profile | ${string} | None |
| linode_tags | Tags for the instances | ${list(string)} | ['appbuzz01'] |
| kubernetes_user_name | Username used for the kubernetes user | ${string} | None |
| kubernetes_user_password | Password for the kubernetes user | ${string} | None |
| worker_count | Number of workers | ${number} | None |

