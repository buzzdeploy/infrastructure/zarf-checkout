[[_TOC_]]

# Zarf Deployment

Used for a one click deployment of [Zarf](https://github.com/defenseunicorns/zarf). 

# GitLab CI Variables

Variables that are needed by the Terraform, Ansible, and Linode CLI at run time that are not 
publicly exposed.

| Variable Name |
| :---: |
| sshkey |
| sshkeypub |
| TF_PASSWORD |
| TF_VAR_archrootpass |
| TF_VAR_kubernetes_user_name |
| TF_VAR_kubernetes_user_password |
| TF_VAR_linode_username |
| TF_VAR_linodeapikey |
| LINODE_CLI_OBJ_ACCESS_KEY |
| LINODE_CLI_OBJ_SECRET_KEY |
| LINODE_CLI_TOKEN |
| gitlab_ci_token |
| gitlab_runner |

This will allow an automated deployment for the container.

# Outputs

A bucket has been hardcoded in the CI called `k3sfiles` that the Ansible inventory and the kubeconfig are saved using a name convention based on the unique number.

Those files will allow access to a freshly stood up K3s multinode cluster Linode with Arch images and Zarf deployed on the cluster. 

# Generated Module Variables
## Modules Called
| Module Name | Source |
| :--- | ---: |
| appCluster | ./appCluster |

## Module Variables
| Variable Name | Variable Description | Type | Default |
| :--- | :--- | :---: | ---: |
| linodeapikey | Linode ApiKey | ${string} | None |
| linode_username | Username to be used as noted in the Linode profile | ${string} | None |
| kubernetes_user_name | Username used for the kubernetes user | ${string} | None |
| kubernetes_user_password | Password for the kubernetes user | ${string} | None |
| archrootpass | Arch Linux Root Password | ${string} | None |
| ssh_key_pub | SSH Public Key | ${string} | None |
| ssh_key | SSH Private Key | ${string} | None |

