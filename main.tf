

module "appCluster" {
  source                   = "./appCluster"
  unique_number            = "10"
  ssh_key_pub              = var.ssh_key_pub
  ssh_key                  = var.ssh_key
  worker_count             = 1
  linode_tags              = ["zarf", "zarf99"]
  linodeapikey             = var.linodeapikey
  kubernetes_user_name     = var.kubernetes_user_name
  kubernetes_user_password = var.kubernetes_user_password
  linode_username          = var.linode_username
  archrootpass             = var.archrootpass
}
