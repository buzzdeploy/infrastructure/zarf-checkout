boto==2.49.0
boto3==1.26.25
botocore==1.29.25
certifi==2022.12.7
charset-normalizer==2.1.1
idna==3.4
jmespath==1.0.1
linode-cli==5.27.2
pyhcl==0.4.4
python-dateutil==2.8.2
python-rclone==0.0.2
PyYAML==6.0
requests==2.28.1
s3transfer==0.6.0
six==1.16.0
terminaltables==3.1.10
urllib3==1.26.13
